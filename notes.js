// console.log('Starting notes.js');

const fs = require('fs');

var fetchNotes = () => {
  try {
    var notesString = fs.readFileSync('notes-data.json');
    return JSON.parse(notesString);
  } catch (e) {
    return [];
  }
};

var saveNotes = (notes) => {
  fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

// node app.js add  --title=secret --body="This is my secret"
// OR
// node app.js add  --title secret --body "This is my secret"
var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body
  };
  var duplicateNotes = notes.filter((note) => note.title === title);

  if (duplicateNotes.length === 0) {
    notes.push(note);
    saveNotes(notes);
    return note;
  }
};

// node app.js list
var listAll = () => {
  return fetchNotes();
};

// node app.js read --title='secret'
// OR
// node app.js read --title secret
var readNote = (title) => {
  var notes = fetchNotes();
  var filteredNotes = notes.filter((note) => note.title === title);
  return filteredNotes[0];
};

// node app.js remove --title='secret'
// OR
// node app.js remove --title secret
var removeNote = (title) => {
  var notes = fetchNotes();
  var filteredNotes = notes.filter((note) => note.title !== title);
  saveNotes(filteredNotes)

  return notes.length !== filteredNotes.length;
};

var logNote = (note) => {
  console.log('----');
  console.log(`Title: ${note.title}`);
  console.log(`Body: ${note.body}`);
};

module.exports = {
  // we can use alternate syntax like following:
  // addNote: addNote,
  // But we are using ES6 syntax like following:
  addNote,
  listAll,
  readNote,
  removeNote,
  logNote
};

//module.exports.age = 25;
//
// module.exports.addNote = () => {
//   console.log('addNote');
//   return 'New note';
// };

// module.exports.add = function(a,b) {
//   return a + b;
// };

// // OR
// module.exports.add = (a, b) => {
//   return a + b;
// };
