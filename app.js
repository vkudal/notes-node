// console.log('Starting app.js');

// const os = require('os'); // The os module provides a number of operating system-related utility methods.
const fs = require('fs'); // The Node.js file system module allow you to work with the file system on your computer.
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes');

const titleOptions = {
  describe: 'Title of note',
  demand: true,
  alias: 't'
}
const bodyOptions = {
  describe: 'Body of note',
  demand: true,
  alias: 'b'
}
const argv = yargs
  .command('add', 'Add a new note', {
    title: titleOptions,
    body: bodyOptions
  })
  .command('list', 'List all notes')
  .command('read', 'Read a note', {
    title: titleOptions
  })
  .command('remove', 'Remove a note', {
    title: titleOptions
  })
  .help()
  .argv
// var command = process.argv[2];
var command = argv._[0];
// console.log('Command: ', command);
// console.log('Process', process.argv);
// console.log('Yagrs', argv);

if (command === 'add') {
  // console.log('Adding new note');
  var note = notes.addNote(argv.title, argv.body);
  if (note) {
    console.log('Note created');
    notes.logNote(note);
  } else {
    console.log('Note title already in use');
  }
} else if (command === 'list') {
  // console.log('Listing all notes');
  var allnotes = notes.listAll();
  console.log(`Printing ${allnotes.length} note(s)`);
  allnotes.forEach((note) => notes.logNote(note))
} else if (command === 'read') {
  // console.log('Reading note');
  note = notes.readNote(argv.title);
  if (note) {
    console.log('Reading Note');
    notes.logNote(note);
  } else {
    console.log('Note not found');
  }
} else if (command === 'remove') {
  // console.log('Removing note');
  noteRemoved = notes.removeNote(argv.title);
  var message = noteRemoved ? ' Note was removed' : ' Note not found'
  console.log(message);
} else {
  console.log('Not recognized');
}

// var user = os.userInfo();
//
// // Option one - with callback
// fs.appendFile('greetings.txt', `Hello ${user.username}! You are ${notes.age}.`, function (err) {
//   if (err) {
//     console.log('Unable to write to a file');
//   }
// });

// // Option two - without callback
// fs.appendFileSync('greetings.txt', 'Hello world!');

// var res = notes.addNote();
// console.log(res);

// var res = notes.add(7, -2);
// console.log(res);
// // OR
// console.log('Result:', notes.add(9, -2));

// console.log(_.isString(true));
// console.log(_.isString('Vitthal'));

// var filteredArray = _.uniq(['Vitthal', 1, 'Vitthal', 1, 2, 3, 4])
// console.log(filteredArray);
