// NOTE: JSON.stringify turns a Javascript object into JSON text and stores that JSON text in a string.
// NOTE: JSON.parse turns a string of JSON text into a Javascript object.

// var obj = {
//   name: 'Vitthal'
// };
// var stringObj = JSON.stringify(obj);
// console.log(typeof stringObj);
// console.log(stringObj);

// var personString = '{"name": "Vitthal", "age": "34"}';
// var person = JSON.parse(personString);
// console.log(typeof person);
// console.log(person);

const fs = require('fs');

var originalNote = {
  title: 'Some title',
  body: 'Some body'
};

var originalNoteString = JSON.stringify(originalNote)
fs.writeFileSync('notes.json', originalNoteString);

var noteString = fs.readFileSync('notes.json')
var note = JSON.parse(noteString);
console.log(typeof note);
console.log(note.title);
