// NOTE: Node.js includes an out-of-process debugging utility accessible via a V8 Inspector and built-in debugging client.
// NOTE: To use it, start Node.js with the inspect argument followed by the path to the script to debug.
// NOTE: On successful launch of the debugger, prompt will be displayed

var person = {
  name: 'Vitthal'
};

person.age = 34;

// NOTE: Inserting the statement debugger; into the source code of a script will enable a breakpoint at that position in the code
debugger;

// NOTE: Type help to see the available commands to debug.
// NOTE: The repl command allows code to be evaluated remotely.

person.name = 'Veets';

console.log(person);
