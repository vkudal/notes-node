// Arrow Functions

// NOTE: An arrow function expression has a shorter syntax than a function expression.
// NOTE: It does not have its own this, arguments, super, or new.target
// NOTE: These function expressions are best suited for non-method functions, and they cannot be used as constructors.

// A simple arrow function
var square = (x) => {
    return x * x;
};
// If the arrow function has only one return statement, the you can define it as specified below
var square = (x) => x * x;
// If there is only one argument then, the arrow function can be also written without enclosing round brackets for the argument.
var square = x => x * x;

console.log(square(9));

var user = {
  name: 'Vitthal',
  sayHi: () => {
    console.log(arguments);
    console.log(`Hi. I'm ${this.name}`);
  },
  sayHiAlt () {
    console.log(arguments);
    console.log(`Hi. I'm ${this.name}`);
  }
};

// user.sayHi();
// user.sayHi(1,2,3);
user.sayHiAlt();
user.sayHiAlt(1,2,3);
